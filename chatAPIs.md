### Database & decryption details: 
database: 'admin'  
collection: 'chatBox'
database struture: 
```json
{
    "_id" : {
        "time" : 1554454364500.0
    },
    "from" : "alice",
    "to" : "bob",
    "ad_id" : 60008,
    "message" : "58a7c3c89a9df453471dcd578bdc1861ab2910",
    "readStatus" : false
}
```
decryption library: `npm i cryptr`  

```javascript
const Cryptr = require('cryptr');
var key='IABfUNof3nMtz3HXFCUaTYTAuWtj8ym4vz4DX6Mpc5XgWMqAudMwHLD9OdkauJPYFbXRVF6MiHEt3jMh9Pi3L3G9dxYPf1t5gMKHepvumBYkhk85ncfe3WP2XdUBi86LB3A6s5zgG4K8I1ubFxFux1';
const cryptr = new Cryptr(key);

var decryptedMessage= cryptr.decrypt(encryptedMessage);
```
# chatBox concept
a chat box is a list of all the messages between sender and receiver for a specific ad.

## /getAllChatBox

user will get all his chat boxes(empty) for all ads.  
sample query `.find({$or:[{"from":"alice"},{"to":"alice"}]},{"ad_id":1,"from":1 ,"to":1})`
#### post data: 
```json
    {
		"user": "alice"	
	}
```

#### response: 
```json
    {
  "success": true,
  "msg": [
    {
      "_id": 63008,
      "chatbox": [
        {
          "to": "bob"
        }
      ]
    },
    {
      "_id": 5920677,
      "chatbox": [
        {
          "to": "bob1"
        },
        {
          "to": "bob"
        }
      ]
    }
  ]
}
```

## /getLastMsgOfChatBox

user will get the last message of a specific chatbox for specific ad which is for a specific user along with ad title.   

#### post data: 
```json
    {
		"from": "alice",
		"to": "bob"
		"ad_id":1234		
	}
```

#### response: 
```json
    {
  "success": true,
  "msg": [
    {
      "from": "bob",
      "to": "alice",
      "message": "hy6",
      "readStatus": true,
      "timestamp": 1554703097575,
      "ad_title": "bob alice"
    }
  ]
}
```

## /getMsgsOfChatBox

user will get the messages of a specific chatbox for specific ad.   
sample query `.find({$and:[{"from":"alice"},{"to":"alice"},{"ad_id":1234}]})`
#### post data: 
```json
    {
		"from": "alice",
		"to": "bob"
		"ad_id":1234		
	}
```

#### response: 
```json
    [
       {
				"from": "from",
				"to": "sender",
				"message": "decrypted message",
				"timestamp": 12132215,
				"readStatus": flase
	   },
       {
				"from": "from",
				"to": "sender",
				"message": "decrypted message",
				"timestamp": 12132215,
				"readStatus": true
	   }
    ]
```


## /updateMessageStatus

user will get the messages of a specific chatbox for specific ad.  

#### post data: 
```json
    {
		"from": "alice",
		"to": "bob",
		"ad_id":1234,
		"time": 1213155212,
		"readStatus": true
	}
```

#### response: 
```json
    {"status": true}
```