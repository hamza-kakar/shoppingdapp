# chatBox concept
a chat box is a list of all the messages between sender and receiver for a specific ad.


## /postchat

user will send message to other user for a specific ad using his private key.  
#### url: `104.156.59.186:4042/postchat`  

#### post data: 
```json
{
	"private_key": "5KaMkY1B2z7KMrkFJXY6FLXYVAu61iJQHE2qbyAcVoxJuEk5XtP",
	"from": "alice",
	"to": "bob1",
	"ad_id":63008,
	"message": "some msg xyz"
	
}
```

## /uploadMedia

user will send message to other user for a specific ad using his private key.  
#### url: `104.156.59.186:4042/uploadMedia`  

#### post data: 

** attache media file in `Content-Type: multipart/form-data` for variable `file`  
the hash returned in response will be used as message in `/postchat`  

#### response: 

```json
{
  "success": true,
  "msg": {
    "hash": "QmU2aCzBmkqTeTxgW89k79HbJ8aQUfaZvCAYdWQTd6wyjA/img"
  }
}

```


## /getAllChatBox

user will get all his chat boxes with their last message and unread message count.  
** skip will be used for pagination. 
** following are the identifiers for `msgType` in meaasge response.  
* `msg` used for text message.
* `img` used for image file (jpg, jpeg, png etc).
* `doc` used for document file(pdf,txt,doc etc).
#### url: `104.156.59.186:4547/getAllChatBox`  

#### post data: 
```json
    {
	"user": "alice",
	"skip": 0
	}
```

#### response: 
```json
{
  "success": true,
  "msg": [
    {
      "recipient": [
        "bob",
        "alice"
      ],
      "ad_id": 5920677,
      "ad": {
        "user": "alice",
        "title": "bob alice",
        "desc": "dsc",
        "price": 20.549999237060547,
        "ipfs_url": "abc.txt",
        "city": "test",
        "cat_id": 1,
        "views": 0,
        "sold_to": "bob",
        "__v": 1
      },
      "unReadCount": 0,
      "lastMsgs": [
        {
          "from": {
            "_id": {
              "user": "bob"
            },
            "fullname": "bob new",
            "ipfs_url": "test.txt",
            "__v": 0
          },
          "to": {
            "_id": {
              "user": "alice"
            },
            "fullname": "alice new",
            "ipfs_url": "test.txt",
            "__v": 0
          },
          "message": "abc",
          "readStatus": true,
          "msgType": "msg",
          "timestamp": 1554814909268
        }
      ]
    }
  ]
}
```

## /getMsgsOfChatBox

user will get latest 25 messages of a specific chatbox and will also mark those messages as read.   
** skip will be used for pagination. 
** following are the identifiers for `msgType` in meaasge response.  
* `msg` used for text message.
* `img` used for image file (jpg, jpeg, png etc).
* `doc` used for document file(pdf,txt,doc etc).
#### url: `104.156.59.186:4547/getMsgsofChatBox`  

#### post data: 
```json
{
	"user": "bob",
	"ad_id": 63008,
	"recipient": [
		"bob",
		"alice"
	],
	"skip": 0
}
```

#### response: 
```json
{
  "success": true,
  "msg": [
    {
      "from": "bob",
      "to": "alice",
      "ad_id": 63008,
      "message": "hy2",
      "readStatus": true,
      "timestamp": 1554797143644,
      "msgType": "msg"
    },
    {
      "from": "alice",
      "to": "bob",
      "ad_id": 63008,
      "message": "hy1",
      "readStatus": true,
      "timestamp": 1554797138634,
      "msgType": "msg"
    }
  ]
}
```

